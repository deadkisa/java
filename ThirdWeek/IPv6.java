import java.util.regex.Pattern;
import java.util.Scanner;

public class IPv6 {
    public static void main(String[] args) {
        Pattern ptr = Pattern.compile("^(([\\da-fA-F]{1,4}):){7}+([\\da-fA-F]{1,4})$", Pattern.CASE_INSENSITIVE);
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите адресс: ");
            String email = vvod.nextLine();
            System.out.println("Адресс " + email + (ptr.matcher(email).matches() ? " правильный" : " неправильный"));
        }

    }
}