import java.util.Scanner;

public class Keyboard {
    public static void main(String[] args) {
        Scanner vvod = new Scanner(System.in);
        System.out.print("Введите слово/предложение(с залипающей клавиатуры): ");
        String word = vvod.nextLine();
        System.out.println("Очищеное от повторов слово/выражение: " + word.replaceAll("(.)\\1{2,}", "$1"));
    }

}
