import java.util.Scanner;

public class Entropy {
    public static void main(String[] args) {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите текст: ");
            String text = vvod.nextLine();
            System.out.printf("Энтропия: %.2f%n", SearchEntropy(text));
        }

    }

    public static double SearchEntropy(String text) {
        double[] counts = new double[Character.MAX_VALUE];

        for (int i = 0; i < text.length(); i++) {
            char symbol = text.charAt(i);
            counts[symbol]++;
        
        }
        int sum = 0;
        for (double count: counts) {
            if (count > 0) sum += count;
        }
        for (int i = 0; i < counts.length; i++){
            if (counts[i] > 0) counts[i] /= sum; 
        }
        double result = 0.0;
        for (double count: counts) {
            if (count > 0) result += count * log2(count);
        }

        return -1 * result;
        
    }
    public static double log2(double x) {
        return Math.log(x) / Math.log(2);
    }
}
