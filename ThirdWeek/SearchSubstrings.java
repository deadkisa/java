import java.util.Scanner;

public class SearchSubstrings {
    public static void main(String args []) {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите исходные строки(через пробел): ");
            String[] str = vvod.nextLine().split(" ");
            System.out.print("Введите искомую подстроку: ");
            String substring = vvod.nextLine();
            System.out.println("Кол-во подстрок: " + SearchTotal(str,substring));
        }
    }

    private static int SearchTotal(String[] strings, String sub){
        int total = 0;
        int i = -1;
        for (String str: strings){
            while ((i = str.indexOf(sub, i+1)) > -1) ++total;
        }
        return total;
    }
}