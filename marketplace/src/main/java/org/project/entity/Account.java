package org.project.entity;

public class Account extends AbstractEntity<Integer> {
    private Integer id;
    private String name;
    
    public Account (String name, Integer id){
        this.id = id;
        this.name = name;
    }
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void getName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "[" + id + "]" + name;
    }
}
