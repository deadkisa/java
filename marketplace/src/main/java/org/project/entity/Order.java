package org.project.entity;

public class Order extends AbstractEntity<Integer> {
    private Integer id;
    private Integer cost;
    private Product product;
    private Account account;

    public Order(Integer id, Integer cost, Product product, Account account) {
        this.id = id;
        this.cost = cost;
        this.product = product;
        this.account = account;
    }
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Product getproduct() {
        return product;
    }

    public void setproduct(Product product) {
        this.product = product;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "[" + id + "] cost =" + cost + " ,product =" + product + " ,account =" + account;
    }
}
