package org.project.entity;

public class Product extends AbstractEntity<Integer> {
    private String name;
    private Integer id;
    private Integer cost;

    public Product (String name, Integer id, Integer cost){
        this.name = name;
        this.id = id;
        this.cost = cost;
    }
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void getName(String name) {
        this.name = name;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
    @Override
    public String toString() {
        return "[" + id + "]" + name + "," + cost;
    }
}