package org.project.repository;

import java.util.ArrayList;

import org.project.entity.Order;

public interface OrderRepository {
    Order findOrderById(Integer id);

    ArrayList<Order> findAllOrders();
}
