package org.project.repository;

import java.util.ArrayList;

import org.project.entity.Account;

public interface AccountRepository {
    Account findAccountById(Integer id);
    
    Account findAccountByName(String name);

    ArrayList<Account> findAllAccounts();
}
