package org.project.repository.impl;

import org.project.entity.Order;
import org.project.repository.AccountRepository;
import org.project.repository.OrderRepository;
import org.project.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class OrderRepositoryImpl implements OrderRepository {
    private static final ArrayList<Order> STATE = new ArrayList<>();
    private static OrderRepository instance;

    static {
        ProductRepository productrepository = ProductRepositoryImpl.getInstance();
        AccountRepository accountrepository = AccountRepositoryImpl.getInstance();
        STATE.addAll(List.of(
                new Order(1, 200, productrepository.findProductById(1), accountrepository.findAccountById(1)),
                new Order(2, 200, productrepository.findProductById(2), accountrepository.findAccountById(2)),
                new Order(3, 10, productrepository.findProductById(3), accountrepository.findAccountById(3))
        ));
    }

    public static synchronized OrderRepository getInstance() {
        if (isNull(instance)) {
            instance = new OrderRepositoryImpl();
        }
        return instance;
    }

    @Override
    public synchronized Order findOrderById(Integer id) {
        for (Order Order : STATE) {
            if (Order.getId() == id)
                return Order;
        }
        return null;
    }

    @Override
    public synchronized ArrayList<Order> findAllOrders() {
        return STATE;
    }

}