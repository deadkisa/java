package org.project.repository.impl;

import org.project.entity.Product;

import org.project.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;

public class ProductRepositoryImpl implements ProductRepository {
    private static final ArrayList<Product> STATE = new ArrayList<>();
    private static ProductRepository instance;

    static {
        STATE.addAll(List.of(
                new Product("mouse", 1, 200),
                new Product("keyboard", 2, 200),
                new Product("glue", 3, 10)
        ));
    }

    public static synchronized ProductRepository getInstance() {
        if (isNull(instance)) {
            instance = new ProductRepositoryImpl();
        }
        return instance;
    }

    @Override
    public synchronized Product findProductById(Integer id) {
        for (Product Product : STATE) {
            if (Product.getId() == id)
                return Product;
        }
        return null;
    }

    @Override
    public synchronized Product findProductByName(String name) {
        for (Product Product : STATE) {
            if (Objects.equals(Product.getName(), name))
                return Product;
        }
        return null;
    }
    @Override
    public Product findProductByCost(String cost) {
        for (Product Product : STATE) {
            if (Objects.equals(Product.getCost(), cost))
                return Product;
        }
        return null;
    }

    @Override
    public synchronized ArrayList<Product> findAllProducts() {
        return STATE;
    }
}