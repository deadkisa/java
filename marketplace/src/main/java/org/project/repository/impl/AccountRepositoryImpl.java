package org.project.repository.impl;

import org.project.entity.Account;

import org.project.repository.AccountRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;

public class AccountRepositoryImpl implements AccountRepository {
    private static final ArrayList<Account> STATE = new ArrayList<>();
    private static AccountRepository instance;

    static {
        STATE.addAll(List.of(
                new Account("David", 1),
                new Account("Andrew", 2),
                new Account("Bo", 3)
        ));
    }

    public static synchronized AccountRepository getInstance() {
        if (isNull(instance)) {
            instance = new AccountRepositoryImpl();
        }
        return instance;
    }

    @Override
    public synchronized Account findAccountById(Integer id) {
        for (Account account : STATE) {
            if (account.getId() == id)
                return account;
        }
        return null;
    }

    @Override
    public synchronized Account findAccountByName(String name) {
        for (Account account : STATE) {
            if (Objects.equals(account.getName(), name))
                return account;
        }
        return null;
    }

    @Override
    public synchronized ArrayList<Account> findAllAccounts() {
        return STATE;
    }

}
