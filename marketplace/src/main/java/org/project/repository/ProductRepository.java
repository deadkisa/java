package org.project.repository;

import java.util.ArrayList;

import org.project.entity.Product;

public interface ProductRepository {
    Product findProductById(Integer id);
    
    Product findProductByName(String name);

    Product findProductByCost(String cost);

    ArrayList<Product> findAllProducts();
}