package week6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import week6.classes.Order;
import week6.classes.Product;
import java.time.LocalDate;
import java.time.Month;

public class OrdersSum {
    public static void main(String[] args) {
        ArrayList<Order> ordersList = getOrders();
        double ordersSum = getOrdersSum(ordersList);
        System.out.println("Сумма заказов за февраль 2020: " + ordersSum);
    }

    private static double getOrdersSum(ArrayList<Order> ordersList) {
        return ordersList.stream()
            .filter(x -> x.getOrderDate().getYear() == 2020  && x.getOrderDate().getMonth().equals(Month.FEBRUARY))
            .flatMap(x -> x.getProducts().stream())
            .mapToDouble(Product::getPrice)
            .sum();
    }

    private static ArrayList<Order> getOrders() {
        ArrayList<Order> ordersList = new ArrayList<>();
        Order order1 = new Order();
        Order order2 = new Order();

        Product product1 = new Product();
        product1.setPrice(40.0);

        Product product2 = new Product();
        product2.setPrice(50.0);

        Product product3 = new Product();
        product3.setPrice(100.0);

        Set<Product> products1 = new HashSet<>();
        products1.add(product1);
        products1.add(product2);
        Set<Product> products2 = new HashSet<>();
        products2.add(product1);
        products2.add(product3);

        order1.setOrderDate(LocalDate.of(2020, 2, 26));
        order1.setProducts(products1);

        order2.setOrderDate(LocalDate.of(2021, 6, 21));
        order2.setProducts(products2);

        ordersList.add(order1);
        ordersList.add(order2);

        return ordersList;
    }
}
