package week6;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileSearch {
    public static void main(String[] args) {
        try (Scanner userInput = new Scanner(System.in)) {
            System.out.println("Искать по названию файла (1) или по содержимому файла (2)?");
            int userChoice = Integer.parseInt(userInput.nextLine());

            System.out.println("Введите путь к директории: ");
            Path pathToDirectory = Paths.get(userInput.nextLine());
            try {
                switch (userChoice) {
                    case 1:
                        System.out.println("Введите название файла: ");
                        String fileNameToSearch = userInput.nextLine();

                        List<File> filesList = searchForFile(pathToDirectory, fileNameToSearch);
                        filesList.forEach(f -> System.out.println(f.getAbsolutePath()));
                        break;

                    case 2:
                        System.out.println("Введите содержимое файла: ");
                        String contentToSearch = userInput.nextLine();

                        List<Path> pathList = findAllFiles(pathToDirectory);
                        for (Path path : pathList) {
                            if (!searchInContent(path.toString(), contentToSearch).isEmpty()) {
                                System.out.println(path); 
                            } 
                        }
                        break;
                }
            }
            catch (IOException e) {
                System.out.println("Something went wrong..");
            }
        }
    }

    public static List<File> searchForFile(Path directoryPath, String fileName) throws IOException{
        return Files
            .walk(directoryPath)
            .map(Path::toFile)
            .filter(File::isFile)
            .filter(f -> f.getName().equals(fileName))
            .collect(Collectors.toList());
    }

    public static List<Path> findAllFiles(Path directoryPath) throws IOException {
        List<Path> pathList = new ArrayList<>();
        try (Stream<Path> stream = Files.walk(directoryPath)) {
            pathList = stream.map(Path::normalize)
                .filter(Files::isRegularFile)
                .filter(path -> path.getFileName().toString().endsWith(".txt"))
                .collect(Collectors.toList());
            return pathList;
        }
    }

    public static List<String> searchInContent(String pathFile, String stringToSearch) throws IOException {
        InputStream inputStream = new FileInputStream(pathFile);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            Stream<String> linesStream = bufferedReader.lines();
            return linesStream
                .filter(a -> a.contains(stringToSearch))
                .collect(Collectors.toList());
        }
    }
}
