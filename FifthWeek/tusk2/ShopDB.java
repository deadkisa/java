package task2;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class ConsumersList {
    public static void main(String[] args) {
        try (Scanner vvod = new Scanner(System.in)) {
            Map<String, Map<String, Integer>> outputMap = new TreeMap<>();
            String input = vvod.nextLine();
            while (!input.isEmpty()) {
                mapper(outputMap, input);
                input = vvod.nextLine();
            }
            ArrayList<String> mapKeys = new ArrayList<>(outputMap.keySet());
            for (int i = 0; i < mapKeys.size(); i++) {
                System.out.println(mapKeys.get(i) + ":");
                outputMap.get(mapKeys.get(i)).forEach((k, v) -> System.out.println((k + " - " + v)));
            }
        }
    }

    static void mapper(Map<String, Map<String, Integer>> outputMap, String input) {
        String[] splitedString = input.split(" ");
        Map<String, Integer> nestedMap;
        if(outputMap.containsKey(splitedString[0])){
            nestedMap = outputMap.get(splitedString[0]);

            if(nestedMap.containsKey(splitedString[1])){
                int quantityOfGoods = nestedMap.get(splitedString[1]);
                nestedMap.put(splitedString[1], quantityOfGoods + Integer.parseInt(splitedString[2]));
            } else {
                nestedMap.put(splitedString[1], Integer.parseInt(splitedString[2]));
            }
        } else {
            nestedMap = new TreeMap<>();
            nestedMap.put(splitedString[1], Integer.parseInt(splitedString[2]));
            outputMap.put(splitedString[0], nestedMap);
        }
    }
}
