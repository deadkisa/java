package task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Dictionary {
    public static void main(String[] args) {
            try (Scanner vvod = new Scanner(System.in)) {
                System.out.println("Введите кол-во описаний: ");
                int n = Integer.parseInt(vvod.nextLine());
                Map<String, ArrayList<String>> inputDict = new HashMap<>();

                    for (int i = 0; i < n; i++) {
                        String[] inputString = vvod.nextLine().split(",? ");
                        ArrayList<String> inputArray = new ArrayList<>();
                        for (int k = 1; k < inputString.length; k++) {
                            inputArray.add(inputString[k]);
                            inputArray.remove("-");
                        }
                        inputDict.put(inputString[0], new ArrayList<>(inputArray));
                    }

                Map<String, ArrayList<String>> outputDict = new TreeMap<String, ArrayList<String>>(Transform(inputDict));
                System.out.println(outputDict.size());
                outputDict.forEach((k, v) -> System.out.println((k + " - " + String.join(", ", v))));
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }

    private static Map<String, ArrayList<String>> Transform(Map<String, ArrayList<String>> data) {
        Map<String, ArrayList<String>> outputDict = new HashMap<>();
        for (Map.Entry<String, ArrayList<String>> entry : data.entrySet()) {
            ArrayList<String> words = new ArrayList<>(entry.getValue());
            for (int i = 0; i < words.size(); i++) {
                ArrayList<String> temp;
                if(outputDict.containsKey(words.get(i))){
                    temp = outputDict.get(words.get(i));
                    temp.add(entry.getKey());
                } else {
                    temp = new ArrayList<String>();
                    temp.add(entry.getKey());
                    outputDict.put(words.get(i), temp);
                }
            }
        }

        return outputDict;
    }
}
