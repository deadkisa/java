package task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Languages {
    public static void main(String[] args) {
        try (Scanner userInput = new Scanner(System.in, "utf-8")) {
            ArrayList<ArrayList<String>> mainArray = new ArrayList<ArrayList<String>>();
            ArrayList<String> tempArray = new ArrayList<String>();
            System.out.print("Введите количество школьников: ");
            int amountOfSchool = userInput.nextInt();
            for (int i = 0; i < amountOfSchool; i++) {
                System.out.print("Введите количество языков: ");
                int amountOfLang = userInput.nextInt();
                System.out.println("Введите языки: ");
                for (int k = 0; k < amountOfLang; k++) {
                    tempArray.add(userInput.next());
                }
                mainArray.add(new ArrayList<>(tempArray));
                tempArray.clear();
            }
            
            HashSet<String> intersection = findIntersection(mainArray);
            HashSet<String> union = findUnion(mainArray);

            String[] intersectionArray = intersection.toArray(new String[0]);
            Arrays.sort(intersectionArray);

            String[] unionArray = union.toArray(new String[0]);
            Arrays.sort(unionArray);

            System.out.println();
            System.out.println(intersectionArray.length);
            for (int i = 0; i < intersectionArray.length; i++) {
                System.out.println(intersectionArray[i]);
            }
            
            System.out.println(unionArray.length);
            for (int i = 0; i < unionArray.length; i++) {
                System.out.println(unionArray[i]);
            }

        }
    }

    static HashSet<String> findIntersection(ArrayList<ArrayList<String>> mainArray) {
        HashSet<String> intersection = new HashSet<>(mainArray.get(0));

        for (int i = 1; i < mainArray.size(); i++) {
            intersection.retainAll(mainArray.get(i));
        }
        
        return intersection;
    }

    static HashSet<String> findUnion(ArrayList<ArrayList<String>> mainArray) {
        HashSet<String> union = new HashSet<>(mainArray.get(0));

        for (int i = 1; i < mainArray.size(); i++) {
            union.addAll(mainArray.get(i));
        }
        
        return union;
    }
}
