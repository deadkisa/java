package task3;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashSet;


public class Duplicates {
    public static void main(String[] args){
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите элементы входного массива(через пробел): ");
            String input = vvod.nextLine();
            String[] inputArray = input.split(" ");
            ArrayList<String> list = new ArrayList<>();
            for (int i = 0; i < inputArray.length; i++) list.add(inputArray[i]);
            
            HashSet<String> set = new HashSet<String>(list);
            System.out.print("Массив без дубликатов: " + set);
        }
        catch (NumberFormatException e) {
            System.out.println(e.getClass());
        }
    }
}
