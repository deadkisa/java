import java.util.ArrayList;
import java.util.Scanner;

public class Permutations {
 
	static ArrayList<ArrayList<Integer>> arrangeList = new ArrayList<ArrayList<Integer>> ();
	static ArrayList<Integer> arraylist = new ArrayList<Integer>();
 
	public static void main(String args[]) {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите числа через пробел: ");
            String[] input = vvod.nextLine().split(" ");
			for (int i = 0; i < input.length; i++) arraylist.add(Integer.parseInt(input[i]));
            perms(0, arraylist.size()-1);
			System.out.println(arrangeList);
        } catch (NumberFormatException e) {
			System.out.println("Ошибка ввода, проверьте данные");
		}
	}

	public static void swap(int k, int i) {
		Integer swap = arraylist.get(k);
		arraylist.set(k, arraylist.get(i));
		arraylist.set(i, swap);
	}
 
	public static void perms(int k, int m) {
		if (k > m) {
			ArrayList<Integer> temp = (ArrayList<Integer>)arraylist.clone();
			arrangeList.add(temp);
		} else {
			for (int i = k; i <= m; i++) {
				swap(k, i);
				perms(k + 1, m);
				swap(k, i);
			}
		}
	}
 
}