import java.util.ArrayList;
import java.util.Scanner;

public class Container {
    public static void main(String args[]) {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите числа через пробел: ");
            String[] input = vvod.nextLine().split(" ");
            ArrayList<Integer> arraylist = new ArrayList<Integer>();
			for (int i = 0; i < input.length; i++) arraylist.add(Integer.parseInt(input[i]));
            System.out.println("Объем: " + SizeContainer(arraylist, arraylist.size()-1));
        } 
	}
    public static int SizeContainer(ArrayList<Integer> list, int height) {
        int x = 0, y = height, temp = 0;
        int width = y - x;
        int size = width * Math.min(list.get(y), list.get(x));
        while (x < y) {
            if (list.get(x) < list.get(y)) {
                x++;
            }
            else {
                y--;
            }
            width = y - x;
            temp = width * Math.min(list.get(y), list.get(x));
            if (temp > size) size = temp;
        }
        return size;
    }
}
