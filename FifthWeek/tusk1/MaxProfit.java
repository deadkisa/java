import java.util.ArrayList;
import java.util.Scanner;

public class MaxProfit {
    public static void main(String[] args) {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите числа через пробел: ");
            String[] input = vvod.nextLine().split(" ");
            ArrayList<Integer> pricelist = new ArrayList<Integer>();
            for (int i = 0; i < input.length; i++) pricelist.add(Integer.parseInt(input[i]));
            System.out.println("Прибыль: " + SearchMaxProfit(pricelist));
        } catch (NumberFormatException e) {
            System.out.println("Ошибка ввода, проверьте данные");;
        }
    }
    public static Integer SearchMaxProfit(ArrayList<Integer> list) {
        int max = 1;
        int min = 0;
        int MaxProfit = 0;
        while (max < list.size()) {
            if (list.get(min) < list.get(max)) {
                int profit = list.get(max) - list.get(min);
                MaxProfit = Math.max(profit, MaxProfit);
            }
            else {
                min = max;
            }
            max++;
        }
        return MaxProfit;
    }
}
