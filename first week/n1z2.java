public class n1z2 {
    public static void main(String args[])
    {
        System.out.println(
            "Тип данных\t  Размер\t  Мин.значение\t\t  Макс.значение\t");
        System.out.println("Byte\t\t" + Byte.SIZE
                           + "\t\t" + Byte.MIN_VALUE
                           + "\t\t\t" + Byte.MAX_VALUE);
        /*Самый маленький целый тип данных в java – byte. Это знаковый 8-битовый тип. 
        2^n = S(n-размер типа данных, S-общее кол-во значений). 2 ^ 8 = 256. 
        Отрицательный числа начинают отсчет с «10000000»(-128)
        «0111 1111» = 127. 128 никак не обозначить, т.к. это будет -128. 
        По такому же принципу считаются и остальные типы данных. */
        System.out.println("Short\t\t" + Short.SIZE
                           + "\t\t" + Short.MIN_VALUE
                           + "\t\t\t" + Short.MAX_VALUE);
        System.out.println("Integer\t\t" + Integer.SIZE
                           + "\t\t" + Integer.MIN_VALUE
                           + "\t\t" + Integer.MAX_VALUE);
        System.out.println("Float\t\t" + Float.SIZE
                           + "\t\t" + Float.MIN_VALUE
                           + "\t\t\t" + Float.MAX_VALUE);
        System.out.println("Long\t\t" + Long.SIZE
                           + "\t\t" + Long.MIN_VALUE + "\t"
                           + Long.MAX_VALUE);
        System.out.println("Double\t\t" + Double.SIZE
                           + "\t\t" + Double.MIN_VALUE
                           + "\t\t" + Short.MAX_VALUE);
        
    }
}