import java.util.Scanner;

public class n1z1 {
	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			System.out.print("Введите свое имя: ");
			String name = in.nextLine();
			System.out.println("Привет, " + name);
		}
	}
}
