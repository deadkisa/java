import java.util.Scanner;

public class n1z3 {
    public static void main(String[] args){
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Введите длину ребра икосаэдра: ");
            float l = in.nextFloat();
            System.out.println("Объем икосаэдра: " + ((5 * (Math.pow(l,3) * (3 + (Math.sqrt(5)))))/12));
        }
    }
}
