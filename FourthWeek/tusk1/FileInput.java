import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class FileInput {
    public static void main(String[] args) throws IOException {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите имя файла: ");
            File file = new File(vvod.nextLine());
            System.out.print("Введите кодировку файла: ");
            String encoding = vvod.nextLine();
            if (file.exists()) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding))) {
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    System.out.println("Неккоректный ввод кодировки файла");;
                }
            }
            else {
                System.out.println("Некорректный ввод имени файла");
            }
        } 
    }
}