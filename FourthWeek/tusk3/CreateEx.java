import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

public class CreateEx {
    public static void main(String[] args) {
        try (Scanner vvod = new Scanner(System.in)) {
            System.out.print("Введите показания датчиков: ");
            String readings = vvod.nextLine();
            System.out.print("Выводить по(1-id, 2-средней температуре): ");
            int mode = vvod.nextInt();
            Integer[][] result = OutputReadings(readings);
            switch (mode) {
                case 1:
                    Arrays.sort(result, Comparator.comparingInt(o -> o[0]));
                    for (int i = 0; i < result.length; i++) 
                    System.out.println(result[i][0] + " " + result[i][1]);
                break;
                case 2:
                    Arrays.sort(result, Comparator.comparingInt(o -> o[1]));
                    for (int i = 0; i < result.length; i++) 
                    System.out.println(result[i][0] + " " + result[i][1]); 
                break;
                }
        } catch (MissingIndicator e) {
            System.out.println(e.getMessage());;
        }
        
    }
    public static Integer[][] OutputReadings(String readings) throws MissingIndicator {
        String[] text = readings.split("@");
        Integer[][] output = new Integer[text.length][2];
        for (int i = 0; i < text.length; i++){
            if (text[i].length() < 3){
                throw new MissingIndicator("У датчика " + text[i].substring(0, 2) + " нет показателей, работа программы остановлена");
            }
            output[i][0] = Integer.parseInt(text[i].substring(0, 2));
            output[i][1] = Integer.parseInt(text[i].substring(2, text[i].length()));
        }
        return output;  
    }
}
