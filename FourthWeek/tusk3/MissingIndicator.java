public class MissingIndicator extends Exception {
    public MissingIndicator(String message){
        super(message);
    }
}