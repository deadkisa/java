import java.util.Scanner;

public class seventh_task {
   public static void main(String[] args) {
    try (Scanner vvod = new Scanner(System.in)) {
        System.out.print("Введите массив(через пробел): ");
        String numbersvvod = vvod.nextLine();
        String[] StringNumbers = numbersvvod.split(" ");
        double[] numbers = new double[StringNumbers.length];
        for (int i = 0; i < StringNumbers.length; i++) {
            numbers[i] = Double.parseDouble(StringNumbers[i]);
        }
        double max = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        int count = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (max == numbers[i]) {
                count ++;
            }
        }
        System.out.println("Максимальное число - " + max + " Кол-во максимальных чисел - " + count);
    }
    
   } 
}
