import java.util.Scanner;

public class second_task {
   public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
        System.out.print("Введите номер месяца и год(через пробел): ");
        String date = in.nextLine();
        String[] dateArray = date.split(" ");
        int month = Integer.parseInt(dateArray[0]);
        int year = Integer.parseInt(dateArray[1]);
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12 -> {
                System.out.println("В месяце 31 день");
            }
            case 4, 6, 9, 11 -> {
                System.out.println("В месяце 30 дней");
            }
            case 2 -> {
                if (year%4==0) {
                    if (year%100==0) {
                        if (year%400==0) {
                            System.out.println("В месяце 29 дней");
                        }
                        else {
                            System.out.println("В месяце 28 дней");
                        }
                    }
                    else {
                        System.out.println("В месяце 29 дней");
                    }
                }
                else {
                    System.out.println("В месяце 28 дней");
                }
            }
            default -> {
                System.out.println("Введены неверные данные");
            }  
        };
    }
    
   } 
}