import java.util.Scanner;

//Хорошей погода считается, если: жарко/тепло, 
//ясно/облачно, ветра нет, влажность средняя
public class third_task {
   public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
        System.out.print("Температура(жарко/тепло/холодно): ");
        String temper = in.nextLine();
        switch(temper){
            case "жарко","тепло" -> {
                System.out.print("Осадки(ясно/облачно/дождь/снег/град): ");
                String precep = in.nextLine();
                switch(precep){
                    case "ясно", "облачно" -> {
                        System.out.print("Ветер(есть/нет): ");
                        String wind = in.nextLine();
                        switch(wind){
                            case "нет" ->{
                                System.out.print("Влажность(высокая/средняя/низкая): ");
                                String humidity = in.nextLine();
                                switch(humidity){
                                    case "средняя" ->{
                                        System.out.println("Да");
                                    }
                                    default -> {
                                        System.out.println("Нет");
                                    }
                                }
                            }
                            default -> {
                            System.out.println("Нет");
                            }   
                        }
                    }
                default -> {
                System.out.println("Нет");
                }
                }
            }
            default -> {
                System.out.println("Нет");
            }
        }
        }
    }
    
   } 
