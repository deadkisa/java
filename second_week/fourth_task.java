import java.util.Scanner;

public class fourth_task {
   public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
        System.out.print("Введите высоту пирамиды: ");
        int height = in.nextInt();
        for (int i = 1; i <= height; i++) {
            System.out.println(StringMultiply(" ", (height - i)) +
            StringMultiply("#", i) + " " + StringMultiply("#", i));

        }
    }
    
   }
   public static String StringMultiply(String s, int n){
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < n; i++){
        sb.append(s);
    }
    return sb.toString();
} 
}
