import java.util.Arrays;
import java.util.Scanner;

public class sixth_task {
   public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
        System.out.print("До какого числа искать простые числа: ");
        int size = in.nextInt();
        var sieve = new boolean[size];
        Arrays.fill(sieve, true);
        sieve[0] = false;
        sieve[1] = false;

        for (int i = 2; i * i < size; i++) {
            if (sieve[i]) {
                for (int j = i * i; j < size; j+= i){
                    sieve[j] = false;
                }
            }
        }
        System.out.print("Простые числа: ");
        for (int i = 2; i < size; i++) {
            if (sieve[i]) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
    
   } 
}