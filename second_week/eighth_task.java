import java.util.Arrays;
import java.util.Scanner;

public class eighth_task {
   public static void main(String[] args) {
    try (Scanner vvod = new Scanner(System.in)) {
        System.out.print("Введите массив(через пробел): ");
        String numbersvvod = vvod.nextLine();
        String[] StringNumbers = numbersvvod.split(" ");
        double[] numbers = new double[StringNumbers.length];
        for (int i = 0; i < StringNumbers.length; i++) {
            numbers[i] = Double.parseDouble(StringNumbers[i]);
        }
        double [] result = sortArray(numbers);
        System.out.println(Arrays.toString(result));

    }
   } 


    public static double [] sortArray(double[] arrayA){ 
        if (arrayA == null) {
            return null;
        }
        if (arrayA.length < 2) {
            return arrayA; 
        }
        double [] arrayB = new double[arrayA.length / 2];
        System.arraycopy(arrayA, 0, arrayB, 0, arrayA.length / 2);

        double [] arrayC = new double[arrayA.length - arrayA.length / 2];
        System.arraycopy(arrayA, arrayA.length / 2, arrayC, 0, arrayA.length - arrayA.length / 2);

        arrayB = sortArray(arrayB);
        arrayC = sortArray(arrayC);

        return mergeArray(arrayB, arrayC);
    }

    public static double[] mergeArray(double[] arrayA, double[] ArrayB) {
        double[] result = new double[arrayA.length + ArrayB.length];
        int aIndex = 0;
        int bIndex = 0;
 
        while (aIndex + bIndex <= result.length){
            if(aIndex == arrayA.length){
                System.arraycopy(ArrayB, bIndex, result, aIndex + bIndex, ArrayB.length - bIndex);
            break;
                }
            if(bIndex == ArrayB.length){
                System.arraycopy(arrayA, aIndex, result, aIndex + bIndex, arrayA.length - aIndex); 
            break;
            }
            if (arrayA[aIndex] < ArrayB[bIndex]) {
                result[aIndex + bIndex] = arrayA[aIndex];
                aIndex++;
                } 
            else {
                result[aIndex + bIndex] = ArrayB[bIndex];
                bIndex++;
                }
        }
        return result;
    }
}