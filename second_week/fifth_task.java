import java.util.Scanner;

public class fifth_task {
   public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
        System.out.print("Введите положительное число: ");
        int number = in.nextDouble();
        geron(number);
    }
    
   }
   public static void geron(int number) {
    if (number < 0) {
        System.out.println("Число не положительное");
    }
    else { 
        int n = number;
        while ((n*n) > number) {
            n = (n + (number/n))/2;
        }
        System.out.println("Квадратный корень числа " + number + " = " + n);
    }
    } 
}
